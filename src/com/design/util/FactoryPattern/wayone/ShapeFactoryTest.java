package com.design.util.FactoryPattern.wayone;

import com.design.util.FactoryPattern.Circle;
import com.design.util.FactoryPattern.Rectangle;
import com.design.util.FactoryPattern.Square;

public class ShapeFactoryTest {

	public static void main(String[] args) {
		/**
		 * 采用强制转换:(反射机制)
		 */
		Rectangle rect = (Rectangle) ShapeFactory.getClass(Rectangle.class);
		rect.draw();
		
		Square square = (Square) ShapeFactory.getClass(Square.class);
		square.draw();
		
		Circle circle=(Circle) ShapeFactory.getClass(Circle.class);
		circle.draw();
	}

}
