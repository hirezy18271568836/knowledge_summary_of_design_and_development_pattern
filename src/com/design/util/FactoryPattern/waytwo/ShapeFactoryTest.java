package com.design.util.FactoryPattern.waytwo;

import com.design.util.FactoryPattern.Circle;
import com.design.util.FactoryPattern.Rectangle;
import com.design.util.FactoryPattern.Square;

public class ShapeFactoryTest {
     /**
      * 省略类型强制转换，支持多态(和第二种方法相比)
      * @param args
      */
	public static void main(String[] args) {
		Rectangle rect = (Rectangle) ShapeFactory.getClass(Rectangle.class);
		rect.draw();
		
		Square square = (Square) ShapeFactory.getClass(Square.class);
		square.draw();
		
		Circle circle=(Circle) ShapeFactory.getClass(Circle.class);
		circle.draw();
	}

}
