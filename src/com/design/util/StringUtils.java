package com.design.util;

import java.util.Scanner;

/**
 * Created by ruiqin.shen
 * 类说明：String相关工具类
 */
public final class StringUtils {

 

    public static String saveTwoDecimal(String str) {
        // 整数部分
        String whole = "";
        // 小数部分
        String small = "";
        String target = "";
        // 先判断str is not null
        if (str.length() > 0) {
            // 在判断是否有小数，如果没有小数，自动加俩位小数（用0补充）
            if (!str.contains(".")) {
                //追加俩个00
                StringBuffer sb = new StringBuffer();
                sb.append(".00");
                str = str + sb.toString();
            }

            if (str.contains(".")) {

                // 截取小数后面的俩位小数，不够的用0补上
                // 先截取整数部分，也就是小数点以前的数
                whole = str.substring(0, str.indexOf("."));
                // 如果整数小于5位
                if (whole.length() < 5) {
                    target = "0." + getNumber(whole) + whole.substring(0, whole.length() - 2) + "万元";
                }
                // 大于10000的要考虑小数的个数
                else {
                  target = whole.substring(0, whole.length() - 4) + "." + whole.substring(whole.length() - 4, whole.length() - 2) + "万元";
                }

            }
            return target;

        }
        return "";

    }

    public static String getNumber(String str) {
        StringBuffer buffer = new StringBuffer();
        //不包含点
        if (!str.equals("")) {
            if (!str.contains(".")) {
                for (int i = 0; i < 2 - (str.length() - 2); i++) {
                    if (str.length() - 2 == 2) {
                        return "";
                    }
                    buffer.append(0);
                }
                return buffer.toString();
            }
            return "";

        } else {
            return "";
        }

    }
    
    public static void main(String[] args) {
    	System.out.println("请输入金额");
    	Scanner scan = new Scanner(System.in);
    	String money=scan.next();
    	System.out.println(StringUtils.saveTwoDecimal(money));
		
	}

}